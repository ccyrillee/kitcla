package forms

import (
	"gitlab.com/ccyrillee/kitcla/components"
	"gitlab.com/ccyrillee/kitcla/components/atoms/buttons"
	"gitlab.com/ccyrillee/kitcla/components/atoms/links"
	"gitlab.com/ccyrillee/kitcla/goc"
)

type DeleteForm struct {
	Button    *buttons.Button
	Link      *links.Link
	Component *components.Component
}

type DeleteFormMod struct {
	ActionUrl string
}

func (this *DeleteForm) DeleteForm(actionUrl string) goc.HTML {
	return this.H(&DeleteFormMod{ActionUrl: actionUrl})
}

func (this *DeleteForm) H(mod *DeleteFormMod) goc.HTML {
	deleteButton := this.Link.SubmitLink("Delete")
	return this.Component.Cas("form", goc.Attr{"action": mod.ActionUrl, "method": "post"}, deleteButton)
}
