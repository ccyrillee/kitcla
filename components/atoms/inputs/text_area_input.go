package inputs

import (
	"gitlab.com/ccyrillee/kitcla/components"
	"gitlab.com/ccyrillee/kitcla/goc"
)

type TextAreaInput struct {
	Component *components.Component
}

type TextAreaInputMod struct {
	Name         string
	Value        string
	ColumnsCount int
	RowsCount    int
}

func (this *TextAreaInput) TextAreaInput(name string, value string, mod *TextAreaInputMod) goc.HTML {
	mod = this.modDefaulting(mod)
	mod.Name = name
	mod.Value = value
	return this.H(mod)
}

func (this *TextAreaInput) modDefaulting(mod *TextAreaInputMod) *TextAreaInputMod {
	if mod == nil {
		return &TextAreaInputMod{}
	}
	return mod
}

func (this *TextAreaInput) Mod() *TextAreaInputMod {
	return &TextAreaInputMod{}
}

func (this *TextAreaInput) H(mod *TextAreaInputMod) goc.HTML {
	h := this.Component.Cav("textarea",
		goc.Attr{"name": mod.Name, "class": "border border-scale-3 px-3 py-2 rounded-md", "rows": "15"},
		mod.Value,
	)
	goc.M(&h, mod)
	return h
}
