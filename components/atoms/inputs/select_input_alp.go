package inputs

import (
	"gitlab.com/ccyrillee/kitcla/components"
	"gitlab.com/ccyrillee/kitcla/goc"
)

type SelectInputAlp struct {
	Component *components.Component
}

type SelectInputAlpMod struct {
	XModel     string
	OnChange   string
	Options    []*SelectOption
	SelectAttr goc.Attr
}

func (this *SelectInputAlp) CreateOptions() []*SelectOption {
	var options []*SelectOption
	return options
}

func (this *SelectInputAlp) AddOption(options []*SelectOption, value string, text string) []*SelectOption {
	option := &SelectOption{
		Value: value,
		Text:  text,
	}
	options = append(options, option)
	return options
}

func (this *SelectInputAlp) AddOptionWithData(options []*SelectOption, value string, text string, data string) []*SelectOption {
	option := &SelectOption{
		Value: value,
		Text:  text,
		Data:  data,
	}
	options = append(options, option)
	return options
}

func (this *SelectInputAlp) SelectInputAlp(xModel string, options []*SelectOption) goc.HTML {
	return this.H(&SelectInputAlpMod{XModel: xModel, Options: options})
}

func (this *SelectInputAlp) SelectInputAlpWithOnChange(xModel string, options []*SelectOption, onChange string) goc.HTML {
	return this.H(&SelectInputAlpMod{XModel: xModel, Options: options, OnChange: onChange})
}

func (this *SelectInputAlp) H(mod *SelectInputAlpMod) goc.HTML {
	attr := mod.SelectAttr
	if attr == nil {
		attr = goc.Attr{}
	}
	attr["x-model"] = mod.XModel
	if mod.OnChange != "" {
		attr["@change"] = mod.OnChange
	}
	attr["class"] = "border border-scale-3 px-3 py-2 bg-scale-0 rounded-md"
	attr["style"] = "height:42px;"
	return this.Component.Cas("select",
		attr,
		this.options(mod)...,
	)
}

func (this *SelectInputAlp) options(mod *SelectInputAlpMod) []goc.HTML {
	var options []goc.HTML
	for _, option := range mod.Options {
		options = append(options, this.option(option, ""))
	}
	return options
}

func (this *SelectInputAlp) option(option *SelectOption, value string) goc.HTML {
	values := goc.Attr{"value": option.Value}
	if option.Value == value {
		values["selected"] = "selected"
	}
	if option.Data != "" {
		values["data-options"] = option.Data
	}
	return this.Component.Cav("option",
		values,
		option.Text,
	)
}
