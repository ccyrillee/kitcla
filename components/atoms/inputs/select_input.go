package inputs

import (
	"gitlab.com/ccyrillee/kitcla/components"
	"gitlab.com/ccyrillee/kitcla/goc"
)

type SelectInput struct {
	Component *components.Component
}

type SelectOption struct {
	Value string
	Text  string
	Data  string
}

type SelectInputMod struct {
	Name       string
	Value      string
	Options    []*SelectOption
	SelectAttr goc.Attr
}

func (this *SelectInput) CreateOptions() []*SelectOption {
	var options []*SelectOption
	return options
}

func (this *SelectInput) AddOption(options []*SelectOption, value string, text string) []*SelectOption {
	option := &SelectOption{
		Value: value,
		Text:  text,
	}
	options = append(options, option)
	return options
}

func (this *SelectInput) AddOptionWithData(options []*SelectOption, value string, text string, data string) []*SelectOption {
	option := &SelectOption{
		Value: value,
		Text:  text,
		Data:  data,
	}
	options = append(options, option)
	return options
}

func (this *SelectInput) SelectInput(name string, value string, options []*SelectOption) goc.HTML {
	return this.H(&SelectInputMod{Name: name, Value: value, Options: options})
}

func (this *SelectInput) H(mod *SelectInputMod) goc.HTML {
	attr := mod.SelectAttr
	if attr == nil {
		attr = goc.Attr{}
	}
	attr["name"] = mod.Name
	attr["value"] = mod.Value
	attr["class"] = "border border-scale-3 px-3 py-2 bg-scale-0 rounded-md"
	attr["style"] = "height:42px;"
	return this.Component.Cas("select",
		attr,
		this.options(mod)...,
	)
}

func (this *SelectInput) options(mod *SelectInputMod) []goc.HTML {
	var options []goc.HTML
	for _, option := range mod.Options {
		options = append(options, this.option(option, mod.Value))
	}
	return options
}

func (this *SelectInput) option(option *SelectOption, value string) goc.HTML {
	values := goc.Attr{"value": option.Value}
	if option.Value == value {
		values["selected"] = "selected"
	}
	if option.Data != "" {
		values["data-options"] = option.Data
	}
	return this.Component.Cav("option",
		values,
		option.Text,
	)
}
