package cells

import (
	"gitlab.com/ccyrillee/kitcla/components"
	"gitlab.com/ccyrillee/kitcla/goc"
)

type LongTextCell struct {
	Component *components.Component
}

type LongTextCellMod struct {
	Value string
}

func (this *LongTextCell) LongTextCell(value string) goc.HTML {

	return this.H(&LongTextCellMod{Value: value})
}

func (this *LongTextCell) H(mod *LongTextCellMod) goc.HTML {
	return this.Component.Cav("a", goc.Attr{
		"class":  "underline",
		"@click": "showTextIntoModal"},
		"Open text")
}
