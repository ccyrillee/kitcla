package cells

import (
	"gitlab.com/ccyrillee/kitcla/components"
	"gitlab.com/ccyrillee/kitcla/components/atoms/icons"
	"gitlab.com/ccyrillee/kitcla/goc"
)

type BooleanCell struct {
	Component *components.Component
}

type BooleanCellMod struct {
	Value      bool
	TrueColor  string
	FalseColor string
}

func (this *BooleanCell) BooleanCell(value bool) goc.HTML {
	return this.H(&BooleanCellMod{Value: value, TrueColor: "success", FalseColor: "warning"})
}

func (this *BooleanCell) NeutralFalseBooleanCell(value bool) goc.HTML {
	return this.H(&BooleanCellMod{Value: value, TrueColor: "success", FalseColor: "scale-6"})
}

func (this *BooleanCell) H(mod *BooleanCellMod) goc.HTML {
	icon := icons.Icon{}
	color := "text-" + mod.TrueColor
	// Embed values for css pruner: text-success, text-warning, text-scale-6
	icon2 := icon.Icon(icons.IconFasCircleCheck)
	if mod.Value == false {
		color = "text-" + mod.FalseColor
		icon2 = icon.Icon(icons.IconFasCircleXmark)

	}
	return this.Component.Ccs("div", "h-full flex items-center", this.Component.W(color, icon2))
}
