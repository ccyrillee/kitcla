package cells

import (
	"gitlab.com/ccyrillee/kitcla/components"
	"gitlab.com/ccyrillee/kitcla/goc"
)

type TextCell struct {
	Component *components.Component
}

type TextCellMod struct {
	Value string
}

func (this *TextCell) TextCell(value string) goc.HTML {
	return this.H(&TextCellMod{Value: value})
}

func (this *TextCell) H(mod *TextCellMod) goc.HTML {
	return this.Component.Cv("span", mod.Value)
}
