tailwind.config = {
    theme: {
        extend: {
            colors: {
                "primary-1": "var(--primary-1)",
                "neutral-1": "var(--neutral-1)",
                "positive-1": "var(--positive-1)",
                "negative-1": "var(--negative-1)",
                "low-1": "var(--low-1)",
                "secondary-1": "var(--secondary-1)",
                success: "var(--success)",
                warning: "var(--warning)",
                error: "var(--error)",
                selection: "var(--selection)",
                pending: "var(--pending)",
                ongoing: "var(--ongoing)",
                "scale-0": "var(--scale-0)",
                "scale-1": "var(--scale-1)",
                "scale-2": "var(--scale-2)",
                "scale-3": "var(--scale-3)",
                "scale-4": "var(--scale-4)",
                "scale-5": "var(--scale-5)",
                "scale-6": "var(--scale-6)",
                "scale-7": "var(--scale-7)",
                "scale-8": "var(--scale-8)",
                "scale-9": "var(--scale-9)",
                "scale-10": "var(--scale-10)",
            },
        }
    }
}