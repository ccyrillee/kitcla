function debug(port) {
    return {
        retryCount: 0,
        beenConnected: false,
        url: 'ws://localhost:' + port + '/ws',
        lostConnection: false,
        showIndicator: false,
        connectWebSocket() {
            let socket = new WebSocket(this.url);
            socket.onopen = (event) => {
                if (this.lostConnection && this.beenConnected) {
                    window.location.reload();
                }
                this.beenConnected = true;
            };

            socket.onclose = (event) => {
                const connectionLossCode = 1006
                // Socket will also close on things like form submission, with code = 1000 in this case
                if (event.code !== connectionLossCode) {
                    return
                }
                console.log("Connection loss")
                this.lostConnection = true;
                if (this.retryCount > 0) {
                    this.showIndicator = true;
                }
                if (this.retryCount > 10) {
                    return;
                }
                this.retryCount++;
                setTimeout(() => this.connectWebSocket(), 500);
            };
        }
    }
}