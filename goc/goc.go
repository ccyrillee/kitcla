package goc

import (
	"encoding/json"
	"fmt"
	"html"
	"math/rand"
	"os"
	"runtime"
	"strconv"
	"strings"
)

var selfClosingTags = map[string]int{
	"area":  1,
	"br":    1,
	"hr":    1,
	"image": 1,
	"input": 1,
	"img":   1,
	"link":  1,
	"meta":  1,
}

// Attr is a HTML element attribute
// <a href="#"> => Attr{"href": "#"}
type Attr map[string]string

type HTML struct {
	El        string
	Attrs     []interface{}
	HTMLDebug *HTMLDebug
}

type HTMLDebug struct {
	JsonProps []byte `json:"json_props"`
	XGocId    string `json:"x_goc_id"`
	Caller    string `json:"caller"`
}

// dangerous contents type
type dangerousContents func() (string, bool)

// UnsafeContent allows injection of JS or HTML from functions
func UnsafeContent(str string) dangerousContents {
	return func() (string, bool) {
		return str, true
	}
}

// H is the base HTML func
func H(el string, attrs ...interface{}) HTML {
	var as []interface{}
	for _, v := range attrs {
		as = append(as, v)
	}
	return HTML{El: el, Attrs: as}
}

func IsDebugMode() bool {
	return os.Getenv("X_GOC") != ""
}

// M Mark the HTML structure for debug capability
func M(h *HTML, props interface{}) {
	if !IsDebugMode() {
		return
	}
	b, err := json.Marshal(props)
	if err != nil {
		panic(err)
	}
	debug := &HTMLDebug{JsonProps: b}
	h.HTMLDebug = debug
	xGocId := rand.Int()
	xGocIdS := strconv.Itoa(xGocId)
	h.HTMLDebug.XGocId = xGocIdS
	_, file, no, ok := runtime.Caller(1)
	if ok {
		debug.Caller = fmt.Sprintf("called from %s#%d\n", file, no)
	}
}

func RenderRoot(root HTML) string {
	return Render(root, root.El, root.Attrs...)
}

func Render(h HTML, el string, attrs ...interface{}) string {
	if el == "" {
		return ""
	}

	var contents []string
	attributes := ""
	for _, v := range attrs {
		switch v := v.(type) {
		case string:
			contents = append(contents, escape(v))
		case Attr:
			attributes = attributes + getAttributes(v)
		case []string:
			children := strings.Join(v, "")
			contents = append(contents, escape(children))
		case []HTML:
			children := subItems(v)
			contents = append(contents, children)
		case HTML:
			contents = append(contents, Render(v, v.El, v.Attrs...))
		case dangerousContents:
			t, _ := v()
			contents = append(contents, t)
		case func() string:
			contents = append(contents, escape(v()))
		default:
			contents = append(contents, escape(fmt.Sprintf("%v", v)))
		}
	}

	if h.HTMLDebug != nil {
		attributes += " data-x-goc='" + h.HTMLDebug.XGocId + "'"
	}

	elc := escape(el)
	if _, ok := selfClosingTags[elc]; ok {
		return "<" + elc + attributes + " />"
	}
	return "<" + elc + attributes + ">" + strings.Join(contents, "") + "</" + elc + ">"
}

func ExportDebug(h HTML, debug []string, attrs ...interface{}) []string {
	if h.HTMLDebug != nil {
		props := h.HTMLDebug.JsonProps
		path := h.HTMLDebug.Caller

		s := "{\"id\": \"" + h.HTMLDebug.XGocId + "\", \"path\": \"" + path + "\", \"data\": " + string(props) + "}"
		debug = append(debug, s)
	}
	for _, v := range attrs {
		switch v := v.(type) {
		case []HTML:
			for _, sub := range v {
				debug = ExportDebug(sub, debug, sub.Attrs...)
			}
		case HTML:
			debug = ExportDebug(v, debug, v.Attrs...)
		}
	}
	return debug
}

func escape(str string) string {
	return html.EscapeString(str)
}

func subItems(attrs []HTML) string {
	var res []string
	for _, v := range attrs {
		res = append(res, Render(v, v.El, v.Attrs...))
	}
	return strings.Join(res, "")
}

func getAttributes(attributes Attr) string {
	var res []string
	for k, v := range attributes {
		pair := fmt.Sprintf("%v='%v'", escape(k), escape(v))
		res = append(res, pair)
	}
	prefix := ""
	if len(res) > 0 {
		prefix = " "
	}
	return prefix + strings.Join(res, " ")
}
