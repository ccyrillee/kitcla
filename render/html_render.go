package render

import (
	"gitlab.com/ccyrillee/kitcla/goc"
	"strings"
)

func Render(html goc.HTML, env string) string {
	start := TemplateStart()
	end := TemplateEnd(env)
	debug := Debug(html)
	return start + css() + goc.RenderRoot(html) + end + debug
}

func css() string {
	return `<style>
	.theme-light {
		--primary-1: #2185d0;
		--secondary-1: #1b1c1d;
		--neutral-1: #e0e1e2;
		--positive-1: #21ba45;
		--negative-1: #db2828;
		--low-1: #ffffff;
		--success: #00C5A4;
		--warning: #F17A3D;
		--error: #DC6150;
		--selection: #93C5FD;
		--pending: #9AA5B1;
		--ongoing: #F17A3D;
		--scale-0: #FFFFFF;
		--scale-1: #F5F7FA;
		--scale-2: #E4E7EB;
		--scale-3: #CBD2D9;
		--scale-4: #9AA5B1;
		--scale-5: #7B8794;
		--scale-6: #616E7C;
		--scale-7: #52606D;
		--scale-8: #3E4C59;
		--scale-9: #323F4B;
		--scale-10: #1F2933;
	}

	.theme-dark {
		--primary-1: #CBD2D9;
		--secondary-1: #865EFF;
		--success: #00b193;
		--warning: #ee6620;
		--error: #DC6150;
		--ongoing: #ee6620;
		--selection: #93C5FD;
		--pending: #616E7C;
		--color-1: #00b193;
		--color-2: #DC6150;
		--color-3: #93C5FD;
		--scale-0: #1F2933;
		--scale-1: #323F4B;
		--scale-2: #3E4C59;
		--scale-3: #52606D;
		--scale-4: #616E7C;
		--scale-5: #7B8794;
		--scale-6: #8B96A3;
		--scale-7: #8B96A3;
		--scale-8: #9AA5B1;
		--scale-9: #9AA5B1;
		--scale-10: #9AA5B1;
	}</style>
	`
}

func TemplateStart() string {
	return `
	<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>QuiK</title>
<link href="/assets/output.css" rel="stylesheet">
<script defer src="/assets-kit/alpine.3.8.1.min.js"></script>
<script src="/assets-kit/paginatedTable.js"></script>
<link rel="icon" href="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%2016%2016'%3E%3Ctext%20x='0'%20y='14'%3E🎋%3C/text%3E%3C/svg%3E" type="image/svg+xml" />
</head>
<body class="theme-light bg-gray-200" style="margin:0;font-family: 'Lato', sans-serif;">
`
}

func liveReload() string {
	return `	<script type="text/javascript">
		let lastTime = null;

		function reloadIfNecessary() {
			const myRequest = new Request('http://localhost:8023/');
			fetch(myRequest)
				.then(response => response.json())
				.then(data => {
					if (lastTime && data.time !== lastTime) {
						console.log("Reloading");
					   location.reload();
					}
					lastTime = data.time;
				})
				.catch(console.error);
		}

		window.setInterval(reloadIfNecessary, 1500);
	</script>`
}

func TemplateEnd(env string) string {
	if env == "dev" {
		return liveReload() + `
	 </body>
</html>
`
	}
	return `
	 </body>
</html>
`
}

func Debug(h goc.HTML) string {
	if !goc.IsDebugMode() {
		return ""
	}

	debug := goc.ExportDebug(h, []string{}, h.Attrs...)
	debugS := "var gocDebug = [\n" + strings.Join(debug, ",\n") + "];"

	event := `
	
	function xGocDebugElement() {
		console.log("Printing Goc debug")
		let nodeList = document.querySelectorAll(":hover");
		let ids = Array.from(nodeList).filter(a => a.dataset.xGoc).map(a => a.dataset.xGoc);
		ids.forEach(id => printXGocDebugElement(id));
	}


	function printXGocDebugElement(id) {
		let el = gocDebug.find(el => (el.id == id.toString()))
		console.log(el.path, el.data);
	}
	
	window.onkeydown = evt => {
	   switch (evt.keyCode) {
	       //F18
	       case 119:
	           xGocDebugElement();
	           break;
	       default:
	           return true;
	   }
	   //Returning false overrides default browser event
	   return false;
	};
	`

	s := "<script>"
	s += event
	s += debugS
	s += "</script>"
	return s
}
