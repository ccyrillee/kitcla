package headers

import (
	"gitlab.com/ccyrillee/kitcla"
	"gitlab.com/ccyrillee/kitcla/goc"
	"gitlab.com/ccyrillee/kitcla/sup"
	"testing"
)

func TestHeaderH1(t *testing.T) {
	kit := kitcla.New()
	component := kit.Atoms.Headers.Header

	h := component.H1("")
	html := goc.RenderRoot(h)

	sup.UpdateEqualHtmlFromDataFile(t, html, "./data/header_h1_1.html")
	sup.AssertEqualHtmlFromDataFile(t, html, "./data/header_h1_1.html")
}

func TestHeaderH2(t *testing.T) {
	kit := kitcla.New()
	component := kit.Atoms.Headers.Header

	h := component.H2("")
	html := goc.RenderRoot(h)

	sup.UpdateEqualHtmlFromDataFile(t, html, "./data/header_h2_1.html")
	sup.AssertEqualHtmlFromDataFile(t, html, "./data/header_h2_1.html")
}
