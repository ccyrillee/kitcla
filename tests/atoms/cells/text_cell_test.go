package cells

import (
	"gitlab.com/ccyrillee/kitcla"
	"gitlab.com/ccyrillee/kitcla/goc"
	"gitlab.com/ccyrillee/kitcla/sup"
	"testing"
)

func TestTextCellTextCell(t *testing.T) {
	kit := kitcla.New()
	component := kit.Atoms.Cells.TextCell

	h := component.TextCell("")
	html := goc.RenderRoot(h)

	sup.UpdateEqualHtmlFromDataFile(t, html, "./data/text_cell_text_cell_1.html")
	sup.AssertEqualHtmlFromDataFile(t, html, "./data/text_cell_text_cell_1.html")
	sup.AddPage("TextCell", html)
}
