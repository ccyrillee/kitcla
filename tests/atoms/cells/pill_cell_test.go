package cells

import (
	"gitlab.com/ccyrillee/kitcla"
	"gitlab.com/ccyrillee/kitcla/goc"
	"gitlab.com/ccyrillee/kitcla/sup"
	"testing"
)

func TestPillCellPillCell(t *testing.T) {
	kit := kitcla.New()
	component := kit.Atoms.Cells.PillCell

	h := component.PillCell("")
	html := goc.RenderRoot(h)

	sup.UpdateEqualHtmlFromDataFile(t, html, "./data/pill_cell_pill_cell_1.html")
	sup.AssertEqualHtmlFromDataFile(t, html, "./data/pill_cell_pill_cell_1.html")
	sup.AddPage("PillCell", html)
}
