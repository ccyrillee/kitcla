package cells

import (
	"gitlab.com/ccyrillee/kitcla"
	"gitlab.com/ccyrillee/kitcla/goc"
	"gitlab.com/ccyrillee/kitcla/sup"
	"testing"
)

func TestDecimalCellDecimalCell(t *testing.T) {
	kit := kitcla.New()
	component := kit.Atoms.Cells.DecimalCell

	h := component.DecimalCell(0.0)
	html := goc.RenderRoot(h)

	sup.UpdateEqualHtmlFromDataFile(t, html, "./data/decimal_cell_decimal_cell_1.html")
	sup.AssertEqualHtmlFromDataFile(t, html, "./data/decimal_cell_decimal_cell_1.html")
	sup.AddPage("DecimalCell", html)
}
