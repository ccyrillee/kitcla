package cells

import (
	"gitlab.com/ccyrillee/kitcla"
	"gitlab.com/ccyrillee/kitcla/goc"
	"gitlab.com/ccyrillee/kitcla/sup"
	"testing"
)

func TestRichTextCellRichTextCell(t *testing.T) {
	kit := kitcla.New()
	component := kit.Atoms.Cells.RichTextCell

	h := component.RichTextCell("")
	html := goc.RenderRoot(h)

	sup.UpdateEqualHtmlFromDataFile(t, html, "./data/rich_text_cell_rich_text_cell_1.html")
	sup.AssertEqualHtmlFromDataFile(t, html, "./data/rich_text_cell_rich_text_cell_1.html")
	sup.AddPage("RichTextCell", html)

}
