package card_bodies

import (
	"gitlab.com/ccyrillee/kitcla"
	"gitlab.com/ccyrillee/kitcla/goc"
	"gitlab.com/ccyrillee/kitcla/sup"
	"testing"
)

func TestDuoCardBodyDuoCardBody(t *testing.T) {
	kit := kitcla.New()
	component := kit.Atoms.CardBodies.DuoCardBody

	h := component.DuoCardBody(nil, nil, nil)
	html := goc.RenderRoot(h)

	sup.UpdateEqualHtmlFromDataFile(t, html, "./data/duo_card_body_duo_card_body_1.html")
	sup.AssertEqualHtmlFromDataFile(t, html, "./data/duo_card_body_duo_card_body_1.html")
}
