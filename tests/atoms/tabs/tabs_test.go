package tabs

import (
	"gitlab.com/ccyrillee/kitcla"
	"gitlab.com/ccyrillee/kitcla/goc"
	"gitlab.com/ccyrillee/kitcla/sup"
	"testing"
)

func TestTabsTab(t *testing.T) {
	kit := kitcla.New()
	component := kit.Atoms.Tabs.Tab

	h := component.Tab(nil)
	html := goc.RenderRoot(h)

	sup.UpdateEqualHtmlFromDataFile(t, html, "./data/tabs_tab_1.html")
	sup.AssertEqualHtmlFromDataFile(t, html, "./data/tabs_tab_1.html")
}
