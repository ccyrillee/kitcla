package resources

import (
	"gitlab.com/ccyrillee/kitcla"
	"gitlab.com/ccyrillee/kitcla/goc"
	"gitlab.com/ccyrillee/kitcla/sup"
	"testing"
)

func TestResourceResourceJsCss(t *testing.T) {
	kit := kitcla.New()
	component := kit.Atoms.Resources.Resource

	h := component.ResourceJsCss("", "")
	html := goc.RenderRoot(h)

	sup.UpdateEqualHtmlFromDataFile(t, html, "./data/resource_resource_js_css_1.html")
	sup.AssertEqualHtmlFromDataFile(t, html, "./data/resource_resource_js_css_1.html")
}
