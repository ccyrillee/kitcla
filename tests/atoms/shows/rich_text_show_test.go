package shows

import (
	"gitlab.com/ccyrillee/kitcla"
	"gitlab.com/ccyrillee/kitcla/goc"
	"gitlab.com/ccyrillee/kitcla/sup"
	"testing"
)

func TestRichTextShowRichTextShow(t *testing.T) {
	kit := kitcla.New()
	component := kit.Atoms.Shows.RichTextShow

	h := component.RichTextShow("", "")
	html := goc.RenderRoot(h)

	sup.UpdateEqualHtmlFromDataFile(t, html, "./data/rich_text_show_rich_text_show_1.html")
	sup.AssertEqualHtmlFromDataFile(t, html, "./data/rich_text_show_rich_text_show_1.html")
}
