package shows

import (
	"gitlab.com/ccyrillee/kitcla"
	"gitlab.com/ccyrillee/kitcla/goc"
	"gitlab.com/ccyrillee/kitcla/sup"
	"testing"
)

func TestTextShowTextShow(t *testing.T) {
	kit := kitcla.New()
	component := kit.Atoms.Shows.TextShow

	h := component.TextShow("", "")
	html := goc.RenderRoot(h)

	sup.UpdateEqualHtmlFromDataFile(t, html, "./data/text_show_text_show_1.html")
	sup.AssertEqualHtmlFromDataFile(t, html, "./data/text_show_text_show_1.html")
}
