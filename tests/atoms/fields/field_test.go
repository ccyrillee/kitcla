package fields

import (
	"gitlab.com/ccyrillee/kitcla"
	"gitlab.com/ccyrillee/kitcla/goc"
	"gitlab.com/ccyrillee/kitcla/sup"
	"gitlab.com/ccyrillee/kitcla/sup/placeholder"
	"testing"
)

func TestFieldField(t *testing.T) {
	kit := kitcla.New()
	component := kit.Atoms.Fields.Field

	h := component.Field("", placeholder.GocHtmlValue())
	html := goc.RenderRoot(h)

	sup.UpdateEqualHtmlFromDataFile(t, html, "./data/field_field_1.html")
	sup.AssertEqualHtmlFromDataFile(t, html, "./data/field_field_1.html")
}

func TestFieldHiddenField(t *testing.T) {
	kit := kitcla.New()
	component := kit.Atoms.Fields.Field

	h := component.HiddenField("", placeholder.GocHtmlValue())
	html := goc.RenderRoot(h)

	sup.UpdateEqualHtmlFromDataFile(t, html, "./data/field_hidden_field_1.html")
	sup.AssertEqualHtmlFromDataFile(t, html, "./data/field_hidden_field_1.html")
}
