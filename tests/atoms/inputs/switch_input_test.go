package inputs

import (
	"gitlab.com/ccyrillee/kitcla"
	"gitlab.com/ccyrillee/kitcla/goc"
	"gitlab.com/ccyrillee/kitcla/sup"
	"testing"
)

func TestSwitchInputSwitchInput(t *testing.T) {
	kit := kitcla.New()
	component := kit.Atoms.Inputs.SwitchInput

	h := component.SwitchInput("", false)
	html := goc.RenderRoot(h)

	sup.UpdateEqualHtmlFromDataFile(t, html, "./data/switch_input_switch_input_1.html")
	sup.AssertEqualHtmlFromDataFile(t, html, "./data/switch_input_switch_input_1.html")
}
