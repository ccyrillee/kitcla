package inputs

import (
	"gitlab.com/ccyrillee/kitcla"
	"gitlab.com/ccyrillee/kitcla/goc"
	"gitlab.com/ccyrillee/kitcla/sup"
	"gitlab.com/ccyrillee/kitcla/sup/placeholder"
	"testing"
)

func TestBooleanInputAlpBooleanInput(t *testing.T) {
	kit := kitcla.New()
	component := kit.Atoms.Inputs.BooleanInputAlp

	h := component.BooleanInput("")
	html := goc.RenderRoot(h)

	sup.UpdateEqualHtmlFromDataFile(t, html, "./data/boolean_input_alp_boolean_input_1.html")
	sup.AssertEqualHtmlFromDataFile(t, html, "./data/boolean_input_alp_boolean_input_1.html")
}

func TestBooleanInputAlpBooleanInputWithAttr(t *testing.T) {
	kit := kitcla.New()
	component := kit.Atoms.Inputs.BooleanInputAlp

	h := component.BooleanInputWithAttr("", placeholder.GocAttrValue())
	html := goc.RenderRoot(h)

	sup.UpdateEqualHtmlFromDataFile(t, html, "./data/boolean_input_alp_boolean_input_with_attr_1.html")
	sup.AssertEqualHtmlFromDataFile(t, html, "./data/boolean_input_alp_boolean_input_with_attr_1.html")
}
