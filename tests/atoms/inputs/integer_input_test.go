package inputs

import (
	"gitlab.com/ccyrillee/kitcla"
	"gitlab.com/ccyrillee/kitcla/goc"
	"gitlab.com/ccyrillee/kitcla/sup"
	"testing"
)

func TestIntegerInputIntegerInput(t *testing.T) {
	kit := kitcla.New()
	component := kit.Atoms.Inputs.IntegerInput

	h := component.IntegerInput("", 0)
	html := goc.RenderRoot(h)

	sup.UpdateEqualHtmlFromDataFile(t, html, "./data/integer_input_integer_input_1.html")
	sup.AssertEqualHtmlFromDataFile(t, html, "./data/integer_input_integer_input_1.html")
}
