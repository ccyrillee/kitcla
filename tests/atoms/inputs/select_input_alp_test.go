package inputs

import (
	"gitlab.com/ccyrillee/kitcla"
	"gitlab.com/ccyrillee/kitcla/goc"
	"gitlab.com/ccyrillee/kitcla/sup"
	"testing"
)

func TestSelectInputAlpSelectInputAlp(t *testing.T) {
	kit := kitcla.New()
	component := kit.Atoms.Inputs.SelectInputAlp

	options := component.CreateOptions()
	h := component.SelectInputAlp("", options)
	html := goc.RenderRoot(h)

	sup.UpdateEqualHtmlFromDataFile(t, html, "./data/select_input_alp_select_input_alp_1.html")
	sup.AssertEqualHtmlFromDataFile(t, html, "./data/select_input_alp_select_input_alp_1.html")
}

func TestSelectInputAlpSelectInputAlpWithOnChange(t *testing.T) {
	//kit := kitcla.New()
	//component := kit.Atoms.Inputs.SelectInputAlp
	//
	//h := component.SelectInputAlpWithOnChange("", "", "")
	//html := goc.RenderRoot(h)
	//
	//sup.UpdateEqualHtmlFromDataFile(t, html, "./data/select_input_alp_select_input_alp_with_on_change_1.html")
	//sup.AssertEqualHtmlFromDataFile(t, html, "./data/select_input_alp_select_input_alp_with_on_change_1.html")
}
