package inputs

import (
	"gitlab.com/ccyrillee/kitcla"
	"gitlab.com/ccyrillee/kitcla/goc"
	"gitlab.com/ccyrillee/kitcla/sup"
	"testing"
)

func TestJsonInputJsonInput(t *testing.T) {
	kit := kitcla.New()
	component := kit.Atoms.Inputs.JsonInput

	h := component.JsonInput("", []byte(""))
	html := goc.RenderRoot(h)

	sup.UpdateEqualHtmlFromDataFile(t, html, "./data/json_input_json_input_1.html")
	sup.AssertEqualHtmlFromDataFile(t, html, "./data/json_input_json_input_1.html")
}
