package inputs

import (
	"gitlab.com/ccyrillee/kitcla"
	"gitlab.com/ccyrillee/kitcla/goc"
	"gitlab.com/ccyrillee/kitcla/sup"
	"testing"
)

func TestTextInputTextInput(t *testing.T) {
	kit := kitcla.New()
	component := kit.Atoms.Inputs.TextInput

	h := component.TextInput("", "")
	html := goc.RenderRoot(h)

	sup.UpdateEqualHtmlFromDataFile(t, html, "./data/text_input_text_input_1.html")
	sup.AssertEqualHtmlFromDataFile(t, html, "./data/text_input_text_input_1.html")
}
