package inputs

import (
	"gitlab.com/ccyrillee/kitcla"
	"gitlab.com/ccyrillee/kitcla/goc"
	"gitlab.com/ccyrillee/kitcla/sup"
	"testing"
)

func TestRichTextInputDeps(t *testing.T) {
	kit := kitcla.New()
	component := kit.Atoms.Inputs.RichTextInput

	h := component.Deps()
	html := goc.RenderRoot(h)

	sup.UpdateEqualHtmlFromDataFile(t, html, "./data/rich_text_input_deps_1.html")
	sup.AssertEqualHtmlFromDataFile(t, html, "./data/rich_text_input_deps_1.html")
}

func TestRichTextInputRichTextInput(t *testing.T) {
	kit := kitcla.New()
	component := kit.Atoms.Inputs.RichTextInput

	h := component.RichTextInput("", "")
	html := goc.RenderRoot(h)

	sup.UpdateEqualHtmlFromDataFile(t, html, "./data/rich_text_input_rich_text_input_1.html")
	sup.AssertEqualHtmlFromDataFile(t, html, "./data/rich_text_input_rich_text_input_1.html")
}
