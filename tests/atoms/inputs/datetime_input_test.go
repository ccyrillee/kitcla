package inputs

import (
	"gitlab.com/ccyrillee/kitcla"
	"gitlab.com/ccyrillee/kitcla/goc"
	"gitlab.com/ccyrillee/kitcla/sup"
	"testing"
)

func TestDatetimeInputDatetimeInput(t *testing.T) {
	kit := kitcla.New()
	component := kit.Atoms.Inputs.DatetimeInput

	h := component.DatetimeInput("", "")
	html := goc.RenderRoot(h)

	sup.UpdateEqualHtmlFromDataFile(t, html, "./data/datetime_input_datetime_input_1.html")
	sup.AssertEqualHtmlFromDataFile(t, html, "./data/datetime_input_datetime_input_1.html")
}
