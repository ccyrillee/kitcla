package card_wrappers

import (
	"gitlab.com/ccyrillee/kitcla"
	"gitlab.com/ccyrillee/kitcla/goc"
	"gitlab.com/ccyrillee/kitcla/sup"
	"gitlab.com/ccyrillee/kitcla/sup/placeholder"
	"testing"
)

func TestCardWrapperCardWrapper(t *testing.T) {
	kit := kitcla.New()
	component := kit.Atoms.CardWrapper.CardWrapper

	h := component.CardWrapper(placeholder.GocHtmlValue())
	html := goc.RenderRoot(h)

	sup.UpdateEqualHtmlFromDataFile(t, html, "./data/card_wrapper_card_wrapper_1.html")
	sup.AssertEqualHtmlFromDataFile(t, html, "./data/card_wrapper_card_wrapper_1.html")
}

func TestCardWrapperCardWrapperWithHeader(t *testing.T) {
	kit := kitcla.New()
	component := kit.Atoms.CardWrapper.CardWrapper

	h := component.CardWrapperWithHeader(placeholder.GocHtmlValue(), placeholder.GocHtmlValue())
	html := goc.RenderRoot(h)

	sup.UpdateEqualHtmlFromDataFile(t, html, "./data/card_wrapper_card_wrapper_with_header_1.html")
	sup.AssertEqualHtmlFromDataFile(t, html, "./data/card_wrapper_card_wrapper_with_header_1.html")
}
