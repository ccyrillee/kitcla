package buttons

import (
	"gitlab.com/ccyrillee/kitcla"
	"gitlab.com/ccyrillee/kitcla/goc"
	"gitlab.com/ccyrillee/kitcla/sup"
	"testing"
)

func TestButtonAlpGhostlyTertiaryIconLink(t *testing.T) {
	kit := kitcla.New()
	component := kit.Atoms.Buttons.ButtonAlp

	h := component.GhostlyTertiaryIconLink("", "")
	html := goc.RenderRoot(h)

	sup.UpdateEqualHtmlFromDataFile(t, html, "./data/button_alp_ghostly_tertiary_icon_link_1.html")
	sup.AssertEqualHtmlFromDataFile(t, html, "./data/button_alp_ghostly_tertiary_icon_link_1.html")
	sup.AddPage("GhostlyTertiaryIconLink", html)
}

func TestButtonAlpPrimaryLink(t *testing.T) {
	kit := kitcla.New()
	component := kit.Atoms.Buttons.ButtonAlp

	h := component.PrimaryLink("", "")
	html := goc.RenderRoot(h)

	sup.UpdateEqualHtmlFromDataFile(t, html, "./data/button_alp_primary_link_1.html")
	sup.AssertEqualHtmlFromDataFile(t, html, "./data/button_alp_primary_link_1.html")
	sup.AddPage("PrimaryLink", html)
}

func TestButtonAlpSecondaryIconLink(t *testing.T) {
	kit := kitcla.New()
	component := kit.Atoms.Buttons.ButtonAlp

	h := component.SecondaryIconLink("", "")
	html := goc.RenderRoot(h)

	sup.UpdateEqualHtmlFromDataFile(t, html, "./data/button_alp_secondary_icon_link_1.html")
	sup.AssertEqualHtmlFromDataFile(t, html, "./data/button_alp_secondary_icon_link_1.html")
	sup.AddPage("SecondaryIconLink", html)
}

func TestButtonAlpSecondaryLink(t *testing.T) {
	kit := kitcla.New()
	component := kit.Atoms.Buttons.ButtonAlp

	h := component.SecondaryLink("", "")
	html := goc.RenderRoot(h)

	sup.UpdateEqualHtmlFromDataFile(t, html, "./data/button_alp_secondary_link_1.html")
	sup.AssertEqualHtmlFromDataFile(t, html, "./data/button_alp_secondary_link_1.html")
	sup.AddPage("SecondaryLink", html)
}

func TestButtonAlpTertiaryIconLink(t *testing.T) {
	kit := kitcla.New()
	component := kit.Atoms.Buttons.ButtonAlp

	h := component.TertiaryIconLink("", "")
	html := goc.RenderRoot(h)

	sup.UpdateEqualHtmlFromDataFile(t, html, "./data/button_alp_tertiary_icon_link_1.html")
	sup.AssertEqualHtmlFromDataFile(t, html, "./data/button_alp_tertiary_icon_link_1.html")
	sup.AddPage("TertiaryIconLink", html)
}
