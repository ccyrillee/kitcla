package buttons

import (
	"gitlab.com/ccyrillee/kitcla"
	"gitlab.com/ccyrillee/kitcla/goc"
	"gitlab.com/ccyrillee/kitcla/sup"
	"testing"
)

func TestButtonPrimaryLink(t *testing.T) {
	kit := kitcla.New()
	component := kit.Atoms.Buttons.Button

	h := component.PrimaryLink("My Label", "test.com")
	html := goc.RenderRoot(h)

	sup.UpdateEqualHtmlFromDataFile(t, html, "./data/button_primary_link_1.html")
	sup.AssertEqualHtmlFromDataFile(t, html, "./data/button_primary_link_1.html")
}
