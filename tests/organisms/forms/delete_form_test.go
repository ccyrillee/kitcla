package forms

import (
	"gitlab.com/ccyrillee/kitcla"
	"gitlab.com/ccyrillee/kitcla/goc"
	"gitlab.com/ccyrillee/kitcla/sup"
	"testing"
)

func TestDeleteFormDeleteForm(t *testing.T) {
	kit := kitcla.New()
	component := kit.Organisms.Forms.DeleteForm

	h := component.DeleteForm("")
	html := goc.RenderRoot(h)

	sup.UpdateEqualHtmlFromDataFile(t, html, "./data/delete_form_delete_form_1.html")
	sup.AssertEqualHtmlFromDataFile(t, html, "./data/delete_form_delete_form_1.html")
}
