package modals

import (
	"gitlab.com/ccyrillee/kitcla"
	"gitlab.com/ccyrillee/kitcla/goc"
	"gitlab.com/ccyrillee/kitcla/sup"
	"testing"
)

func TestModalSimpleModal(t *testing.T) {
	kit := kitcla.New()
	component := kit.Organisms.Modals.Modal

	h := component.SimpleModal()
	html := goc.RenderRoot(h)

	sup.UpdateEqualHtmlFromDataFile(t, html, "./data/modal_simple_modal_1.html")
	sup.AssertEqualHtmlFromDataFile(t, html, "./data/modal_simple_modal_1.html")
}
